//
//  ViewController.swift
//  deberInterfaces
//
//  Created by user1 on 3/7/17.
//  Copyright © 2017 user1. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var createButton: UIButton!
    
  
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        createButton.layer.borderColor=UIColor.init(red: 140.0/255.0, green: 172.0/255.0, blue: 200.0/255.0, alpha: 1.0).cgColor
        
        self.navigationItem.backBarButtonItem=UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

